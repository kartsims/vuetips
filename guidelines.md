---
layout: page
title: Guidelines
permalink: /guidelines
---

* Help solving a single pain point that Vue.js devs may encounter.

* Code samples in ES2015. Add ES5 code in beginner posts only.

* Posts should be between 1000 and 2000 characters long. Try to [KISS](https://en.wikipedia.org/wiki/KISS_principle).

* Adjust the examples and text to the targeted dev skills :
  - Beginner : New to Vue.js, may have read the docs.
  - Intermediate : Knows all the docs, starts tweaking the framework.
  - Confirmed : Knows the docs and the core source code.
