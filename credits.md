---
layout: page
title: Credits
permalink: /credits
---

This blog is updated by [Simon Tarchichi](https://github.com/kartsims).

No external contribution yet, but they are [most welcome](/submit) ! Contributors' names will appear right there, eventually.

## Thanks to

- [Evan You](http://evanyou.me/) for making this awesome JS framework
- The [Jekyll team](https://jekyllrb.com/) for the great blog editing tool
- [John Otander](http://johnotander.com/) for crafting the beautiful [Pixyll](https://github.com/johnotander/pixyll) theme
