## Multi-component app without Webpack or Browserify

Write all your components code in a single HTML file

Expliquer avec les `<script type="x-template">` tags


## Handle SVGs in Vue.js

SVGs are XML. Vue.js handles XML. This is going to be fun.



## Server-side rendered app using Nuxt.js

Feel the beauty of isomorphic JS applications with Vue.js 2.

> https://github.com/nuxt/nuxt.js


## How does Vue.js reactive magic happen ?

Learn how Vue.js properties work

https://monterail.com/blog/2016/how-to-build-a-reactive-engine-in-javascript-part-1-observable-objects
