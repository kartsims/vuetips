---
layout: post
title: "Use a global event bus instead of a state manager"
summary: "Emit and listen to events across every Vue components of your app"
date: 2017-03-08 08:16
tags:
- state-management
permalink: /global-event-bus
---

*Disclaimer : This tip is (largely) inspired by [this post](https://devblog.digimondo.io/building-a-simple-eventbus-in-vue-js-64b70fb90834) by Arvid Kahl*

## What is a global event bus ?

In Vue.js, components are supposed to interact through [Parent-Child Communication](http://vuejs.org/v2/guide/components.html#Composing-Components). It should go *props down, events up* (down being parent to child).

Most applications require components to communicate with other components than their parent or children. The recommended way to do so is to implement a state management solution such as  [Vuex](https://vuex.vuejs.org/en/intro.html) (an official, supported [Flux-like implementation](http://vuejs.org/v2/guide/state-management.html#Official-Flux-Like-Implementation)) or even [VueStash](http://vuetips.com/simple-state-management-vue-stash).

However this may come with a bit of overhead for relatively simple apps, where you have only a few cases of cross-components communication. This is where you could use a *global event bus*.

**Global** because it is available everywhere in your app.

**Event** because it is an event-based system, similar to Javascript's event implementation (similar, but different).

**Bus** is the [term used in computing](https://en.wikipedia.org/wiki/Bus_(computing)) for data transfer between components (for instance between your graphics card and the mother board, you use a PCI bus).

Again, *this is not the recommended way to do things in most applications*. It is mentioned [in the official docs](http://vuejs.org/v2/guide/components.html#Non-Parent-Child-Communication) though, but keep in mind that if you use it **in a complex application, you'll most likely end up with a big event mess**.

## Event bus instantiation

The event bus is just a Vue component. It should be a dedicated component, even if any component could be used (your root component for instance).

```javascript
const EventBus = new Vue()
```

To make it available everywhere in your app, you may attach it to the Vue global object.

```javascript
const EventBus = new Vue()

Object.defineProperties(Vue.prototype, {
  $bus: {
    get: function () {
      return EventBus
    }
  }
})
```

## Trigger global events

You can now trigger events in your components :

```javascript
export default {
  name: 'my-component',
  methods: {
    triggerMyEvent () {
      this.$bus.$emit('my-event', { ... pass some event data ... })
    }
  }
}
```

Or directly in your HTML template

```html
<div>
  <button @click="$bus.$emit('my-event')">Click to trigger event</button>
</div>
```

## Listen to events

Events can be listened to in any component.

```javascript
export default {
  name: 'my-component',
  created () {
    this.$bus.$on('my-event', ($event) => {
      console.log('My event has been triggered', $event)
    })
  }
}
```

Including the event bus component itself

```javascript
const EventBus = new Vue({
  created () {
    this.$on('my-event', this.handleMyEvent)
  },
  methods: {
    handleMyEvent ($event) {
      console.log('My event caught in global event bus', $event)
    }
  }
})
```

## Links

- [Original post by Arvid Kahl](https://devblog.digimondo.io/building-a-simple-eventbus-in-vue-js-64b70fb90834)
- [Vue.js docs](http://vuejs.org/v2/guide/components.html#Non-Parent-Child-Communication) about "Non Parent-Child Communication"
