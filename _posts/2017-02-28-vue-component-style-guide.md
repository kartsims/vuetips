---
layout: post
title: Vue.js Component Style Guide
summary: "General purpose recommandations for writing Vue.js components code"
date: 2017-02-28 07:09
permalink: /vue-component-style-guide
---

> **This post is outdated**. <br/>An official Vue.js style guide is now available on [vuejs.org](https://vuejs.org/v2/style-guide/)

If you have started Vue.js development recently, or even component-based JS frameworks, you have just landed in a new world. You might be a bit lost, and as most developers, you'd rather get started writing code than reading all the docs.

Now that you have experimented a bit, **it is time to write better components** : easily re-usable, clean code and respectful of Vue.js best practices.

[Vue.js component style guide](https://github.com/pablohpsilva/vuejs-component-style-guide) is a set of recommandations that mixes **common best practices** and **opinionated code style rules** to make the Vue.js ecosystem more consistent.

## Example 1 : keep expressions simple

This one is easy, and you are probably doing it already. But a reminder doesn't hurt : **Do not write long expressions in templates, use computed properties instead**. [link](https://github.com/pablohpsilva/vuejs-component-style-guide#keep-expressions-simple)

## Example 2 : make your component's props API rock-solid

Two things to do here : [keep props primitive](https://github.com/pablohpsilva/vuejs-component-style-guide#keep-component-props-primitive) and [harness them](https://github.com/pablohpsilva/vuejs-component-style-guide#harness-your-component-props).

Keeping options primitive will prevent fellow developers from passing reactive or complex objects to components that might lead to behavior inconsistency.

Making sure every properties have a `default` fallback function and a `type` attribute also prevents parent components from passing incorrect values that could lead to potential JS runtime errors.

## Many more to see

Check out the [Github repo](https://github.com/pablohpsilva/vuejs-component-style-guide) and take the most out of it.

Remember that **this guide is meant to be complimentary to the official docs**. Docs are still the "source of truth", but following these recommandations will help you and your team (or the open source world, if you share your code) to write code that is better and easier to maintain.
