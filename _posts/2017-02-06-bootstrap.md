---
layout: post
title: Using Bootstrap with Vue.js
summary: "Question is : do you really need Boostrap ?"
date: 2017-02-06
level: beginner
tags:
- bootstrap
permalink: /bootstrap
---

Bootstrap contains CSS and JS files. CSS can be used without JS, but the other way round makes no sense.

## Use Bootstrap CSS with Vue.js

This is easy. In a "classic" setup (no module bundler) just include the `<link>` tag, as usual.

If you are using the Webpack template, you may use LESS or SASS, depending on your preference :

### LESS version

```
npm install --save-dev bootstrap less less-loader
```

And include the LESS file in `src/main.js`

```
require('../node_modules/bootstrap/less/bootstrap.less')
```

### SASS version

```
npm install --save-dev bootstrap-sass node-sass sass-loader
```

And include the SASS file in `src/main.js`

```
require('../node_modules/bootstrap-sass/assets/stylesheets/_bootstrap.scss')
```

## Use Bootstrap JS with Vue.js

Now this is fairly more complex and calls for other questions.

### *Do I really need Bootstrap ?*

In most cases the answer is no. Bootstrap's JS is based on jQuery, and [jQuery is probably not needed either](/jquery-vs-vue-js). **I won't describe here how to install Bootstrap JS because it just doesn't feel right**. There are alternatives and using a global JS components library is not really the way you should be thinking when working with a component-based framework such as Vue.js

### *What do I need Bootstrap for ?*

One thing that comes often is *I like the way it looks and I already know its API*. If this is the main reason you want to stick with Bootstrap, it's a good time to get out of your comfort zone. Bootstrap JS is not meant for you anymore, you have in your hands a powerful framework surrounded by a rich ecosystem, just use it.

Most people don't use each and every Bootstrap JS components available. So you could start by listing the components you need, and find an alternative. Here are a few ideas of place where you may find what you need :

#### ElementUI

Very nice-looking, complete library. It is mostly a UI framework, but components can be used separately. Check out [its website](http://element.eleme.io), includes a showcase of every component.

Very professionnal looking, and a nice guide. Native language is chinese.

#### Bulma

[Bulma](http://bulma.io) is another UI library. This one is more an alternative to Bootstrap but [@fundon](https://github.com/fundon) has been working on Vue.js components that come ready-to-go.

Check out [`vue-bulma`](https://github.com/vue-bulma) project page on Github. It also contains this sweet Admin template for Vue.js named [`vue-admin`](https://github.com/vue-bulma/vue-admin).

#### Find the `vue-xxx` version of your favorite library

The good thing about component-based framework is, well there are components available out there that don't require you to write any code (beside including the component obviously).

Most big projects have their `vue-xxx` version available out there. Amongst the ones I use regularly, there are [`vue-moment`](https://github.com/brockpetrie/vue-moment) and [`vue-cleave`](https://github.com/jrainlau/vue-cleave) that come in very handy and simple to use.

#### Vue.js components resources

I like to check out [Vue.js newsletter](https://www.getrevue.co/profile/vuenewsletter), [Twitter](https://twitter.com/search?q=%23VueJS) and the [`awesome-vue`](https://github.com/vuejs/awesome-vue) repository for the freshest updates.

If you know websites dedicated to Vue.js components, please post it in the comments.

#### Nothing found ? Just code it

You may not have found the component you are looking for. In the case of Bootstrap component, most of them are fairly easy to do yourself.

I found myself coding an autocomplete component and it was surprinsingly easy. Its code is not relevant and now outdated but start considering the component you need and think how you would do it if you had to code it yourself.

If it seems easy enough, just do it ! It may end up more complicated than planned but well, you'll learn on the way.

## Links

- [`bootstrap-vue`](https://github.com/bootstrap-vue/bootstrap-vue) provides Bootstrap 4 components (2/2/2017: Boostrap v4 is still in Alpha release)
- [Bulma](http://bulma.io/) is a CSS framework
- [`vue-strap`](https://github.com/yuche/vue-strap) are Bootstrap Vue.js components (2/2/2017: still on its way to Vue.js 2.x)

## Feedback

If you are still going to include Bootstrap JS in your app, please tell me why in the comments.

This content may become outdated or irrelevant so please let me know so that I can update it.
