---
layout: post
title: "Hybrid Vue.js app in Cordova : desktop or mobile ?"
summary: A simple hack to know if the code is executed in a website or in the Cordova version of your app
date: 2017-01-06
level: beginner
tags:
- cordova
permalink: /vuejs-cordova-hack
---

If you are developing an hybrid Cordova app that shares the same code in its desktop and mobile version, here is a little hack to know if the code is executed in Cordova.

Files in Cordova apps are served over `file://`, whereas website are served over `http://` or `https://`.

You can access the current protocol using `window.location.protocol`. Here is a code snippet to test it :

```javascript
import Vue from 'vue'
Vue.isCordova = window.location.protocol.match(/^file/)

Vue.extend(
  created () {
    if (this.isCordova) {
      // Yes this is the Cordova version of your app
    } else {
      // This is the desktop version
    }
  }
)
```

Note : Electron apps are also served via `file://` so if your app is also bundled with Electron, `isCordova` will be true.

## Links

- [vue-cordova](https://github.com/kartsims/vue-cordova) : Vue.js wrapper for Cordova API and Cordova plugins
