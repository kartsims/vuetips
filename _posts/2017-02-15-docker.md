---
layout: post
title: "How to use Docker containers for Vue.js applications"
summary: "Get started with Docker and Vue.js"
date: 2017-02-15 10:00
tags:
- devtools
permalink: /use-docker-containers
---

Docker is a containerization technology. It allows one operating system to host different environments, without using virtual machines. It is also **cross-platform : containers can be run in Linux, OSX and Windows**.

## Why would I use Docker ?

With Docker, you can :

1. upgrade your local Node version without risking to break your app's dependencies (and avoid using clever hacks to make sure each project uses the correct Node version)
1. be 100% sure that all developers of your team are using the same version of Node, even across different operating systems (prevents NPM dependencies issues)
1. *build* your app in the exact same, secure, environment

So you have figured it already, **Docker lets you work in a controlled environment** with the right Node, NPM and dependencies versions.

## Get started

First, make sure [Docker is installed](https://docs.docker.com/engine/installation/), obviously.

### Development mode

```bash
docker run --rm -v $(pwd):/src -w /src -p 8080:8080 node:6 bash -c "npm install && npm run dev"
```

Stop the container with `Ctrl`+`C`

### Build for production

```bash
docker run --rm -v $(pwd):/src -w /src node:6 bash -c "npm install && npm run build"
```

### Command breakout

`--rm` tells Docker to delete the container when it is stopped (otherwise you'll end up with a new container every time you start or build your app).

`node:6` is the name of the image.

`-v .:/src` mounts the current directory (`.`) at the root of the container in a folder named `/src`. This is very similar to a virtual drive (Windows) or a virtual disk (Un*x/OSX).

`-w /src` sets the container's working directory (where commands will be executed) at the folder we just created.

`-p 8080:8080` opens the port 8080 of the container to your local browser (needed by Webpack dev server)

`bash -c "..."` starts a new bash instance inside the container, useful for such complicated bash commands using an `&&` that may be misinterpreted by your system's shell.

### Tips

These commands are very long to type but you can make a bash alias (a "shortcut") by placing it in your `.bashrc` file ([Linux](http://www.hostingadvice.com/how-to/set-command-aliases-linuxubuntudebian/)) or `.bash_profile` ([OSX](https://coolestguidesontheplanet.com/make-an-alias-in-bash-shell-in-os-x-terminal/)).

```bash
alias docker-vue-dev='docker run --rm -v $(pwd):/src -w /src -p 8080:8080 node:6 bash -c "npm install && npm run dev"'
alias docker-vue-build='docker run --rm -v $(pwd):/src -w /src node:6 bash -c "npm install && npm run build"'
```

Make sure to delete the `node_modules` folder if you have run `npm install` prior to using docker.

You can change the container's Node version by replacing `node:6` with `node:7` or `node:latest`. Full list of image tags available [here](https://hub.docker.com/_/node/).

You can specify environment variables by changing the command to `npm install && ENV=staging npm run build`

Using Yarn won't speed up the process that much, as it is faster mostly when NPM modules are cached.

On first run, Docker will throw an error such as `Unable to find image 'node:6' locally` and will start pulling the image. This is fine, and once an image is pulled it remains on your disk. You can list available images by typing `docker images` in the terminal.

## Notes

We just scratched the surface of the Docker world. There is a *lot* more to it.

There is no point in running a Vue.js app in a container as it is meant to be run in a web browser, except during development where Node is used as a web server, or during build where Vue.js tools prepare the final JS code.

Docker concepts are your gateway to implement **continuous delivery**, integration or just **automated builds**... Go further and implement [Continuous Integration of your Vue.js app with Gitlab](http://vuetips.com/introduction-continuous-integration-gitlab).
