---
layout: post
title: Another way to declare Vuex modules
summary: "`vuex-module` provides a syntax to write your store modules' state, actions, getters, etc. You may want to use it."
date: 2017-01-14
level: intermediate
tags:
- state management
permalink: /vuex-module-syntax
---

If your app has a complex state management system, and is based on `vuex`, your store probably contains several Vuex modules. If not, [it should](https://vuex.vuejs.org/en/modules.html).

`vuex-module` is a way to rewrite your store modules. You will either like it for its most simple syntax or hate it for being an additional/confusing layer to your stack... Maybe you should at least give it a try.

## `vuex-module` syntax example

This is the sample from the Github repository :

```javascript
import { VuexModule } from 'vuex-module'
import shop from '../../api/shop'

const { state, getter, action, mutation, build } = new VuexModule('products')

state({ all: [] })

getter(function all() {
  return this.state.all
});

action(function fetch() {
  return shop.getProducts(products => {
    this.commit('fetch', products)
  })
})

mutation(function fetch(products) {
  this.state.all = products
})

export default build()
```

And in the component :

```javascript
export default {
  computed: mapGetters({ products: 'all' }, 'products'),
  methods: mapActions(['fetch'], 'products')
}
```

## Links

- [*vuex-module* on Github](https://github.com/underscope/vuex-module)
